﻿using SunMaria.Areas.Item.Interfaces;
using SunMaria.Areas.Item.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace SunMaria.Areas.Item.Repository
{
    public class ItemRepository : IAllItems
    {
        private readonly ItemContext itemContext;
        public ItemRepository(ItemContext itemContext)
        {
            this.itemContext = itemContext;
        }

        public IEnumerable<Items>  Items => itemContext.Item.Include(c => c.Categories);
        Items IAllItems.getObjectItem(int itemsID) => itemContext.Item.FirstOrDefault(p => p.Id == itemsID);
        
    }
}