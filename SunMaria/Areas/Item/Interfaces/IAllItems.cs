﻿using SunMaria.Areas.Item.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunMaria.Areas.Item.Interfaces
{
    public interface IAllItems
    {
        IEnumerable<Items> Items { get;}
        Items getObjectItem(int ItemsID);
    }
}
