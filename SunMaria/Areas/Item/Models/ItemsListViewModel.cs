﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SunMaria.Areas.Item.Models
{
    public class ItemsListViewModel
    {
        public IEnumerable<Items> AllItems { get; set; }
        public string currCategory { get; set; }
        public int Ids { get; set; }
    }
}