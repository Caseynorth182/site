﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SunMaria.Areas.Item.Models
{
    public class Items
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Collection { get; set; }
        public string ForWhom { get; set; }
        public string Age { get; set; }
        public string TypeCanvas { get; set; }
        public string Warehouse { get; set; }
        public string Care { get; set; }
        public string Season { get; set; }
        public int Price { get; set; }
        public string Description { get; set; }
        public bool IsAvailable { get; set; }
        public int CategoriesID { get; set; }   
        public virtual List<Photos> Photo { get; set; }
        public Categories Categories { get; set; }

    }
    public class Categories
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Items> Items { get; set; }
    }
    public class Photos
    {
        public int Id { get; set; }
        [ForeignKey("Items")]
        public int ItemId { get; set; }
        public string Name { get; set; }
        public Items Items { get; set; }
    }
}