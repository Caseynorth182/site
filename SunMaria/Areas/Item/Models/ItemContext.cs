﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SunMaria.Areas.Item.Models
{
    public class ItemContext : DbContext
    {
        public ItemContext() :
            base("DefaultConnection")
        { }

        public DbSet<Items> Item { get; set; }
        public DbSet<Categories> Categories { get; set; }
        public DbSet<Photos> Photos { get; set; }
    }
    //public class UserDbInitializer : DropCreateDatabaseAlways<ItemContext>
    //{
    //    public static void Initial()
    //    {
    //        using (ItemContext db = new ItemContext())
    //        {
    //            if (!db.Categories.Any())
    //                db.Categories.AddRange(Categories.Select(c => c.Value));
    //            if (!db.Item.Any())
    //            {
    //                db.Item.Add(new Items
    //                {
    //                    Id = 1,
    //                    Name = "Admin",
    //                    Collection = "Зима",
    //                    ForWhom = "MEN",
    //                    Age = "11",
    //                    TypeCanvas = "nitka",
    //                    Warehouse = "xz",
    //                    Care = "2xz2",
    //                    Season = "WINTER",
    //                    Price = 250,
    //                    Description = "DECS",
    //                    Categories = Categories["Футболки"]
    //                }); 
    //            }
    //            db.SaveChanges();
    //        }
    //    }
    //    private static Dictionary<string, Categories> category;
    //    public static Dictionary<string, Categories> Categories
    //    {
    //        get
    //        {
    //            if (category == null)
    //            {
    //                var list = new Categories[]
    //                {
    //                    new Categories{ Name = "Футболки" },
    //                    new Categories{ Name = "Штаны" }
    //                };
    //                category = new Dictionary<string, Categories>();
    //                foreach (Categories el in list)
    //                    category.Add(el.Name, el);
    //            }
    //            return category;
    //        }
    //    }
    //}

}
