﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SunMaria.Areas.Item.Models
{
    public class ShopCartItem
    {
        public int id { get; set; }
        public Items item { get; set; }
        public uint price { get; set; }

        public string ShopCartId { get; set; }
    }
}