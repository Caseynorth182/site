﻿using System.Web.Mvc;

namespace SunMaria.Areas.Item
{
    public class ItemAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Item";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
               "Item_default",
               "Item/{contoller}",
               new { action = "List" }
           );
            context.MapRoute(
                "ItemId",
                "Item/{contoller}/{id}",
                new { action = "Id" }
            );
           
        }
    }
}