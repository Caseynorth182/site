﻿using Autofac;
using Autofac.Integration.Mvc;
using SunMaria;
using SunMaria.Areas.Item.Interfaces;
using SunMaria.Areas.Item.Models;
using SunMaria.Areas.Item.Repository;
using System.Web.Mvc;

public class AutofacConfig
{
    public static void ConfigureContainer()
    {
        // получаем экземпляр контейнера
        var builder = new ContainerBuilder();

        // регистрируем контроллер в текущей сборке
        builder.RegisterControllers(typeof(MvcApplication).Assembly);


        // регистрируем споставление типов
        builder.RegisterType<ItemRepository>().As<IAllItems>().WithParameter("itemContext", new ItemContext());
        builder.RegisterType<CategoryRepository>().As<IItemsCategory>().WithParameter("itemContext", new ItemContext());

        // создаем новый контейнер с теми зависимостями, которые определены выше
        var container = builder.Build();

        // установка сопоставителя зависимостей
        DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
    }
}