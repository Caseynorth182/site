﻿namespace SunMaria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhotosItemslist5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Photos", "Items_Id", "dbo.Items");
            DropIndex("dbo.Photos", new[] { "Items_Id" });
            DropColumn("dbo.Photos", "ItemId");
            RenameColumn(table: "dbo.Photos", name: "Items_Id", newName: "ItemId");
            AlterColumn("dbo.Photos", "ItemId", c => c.Int(nullable: false));
            CreateIndex("dbo.Photos", "ItemId");
            AddForeignKey("dbo.Photos", "ItemId", "dbo.Items", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Photos", "ItemId", "dbo.Items");
            DropIndex("dbo.Photos", new[] { "ItemId" });
            AlterColumn("dbo.Photos", "ItemId", c => c.Int());
            RenameColumn(table: "dbo.Photos", name: "ItemId", newName: "Items_Id");
            AddColumn("dbo.Photos", "ItemId", c => c.Int(nullable: false));
            CreateIndex("dbo.Photos", "Items_Id");
            AddForeignKey("dbo.Photos", "Items_Id", "dbo.Items", "Id");
        }
    }
}
