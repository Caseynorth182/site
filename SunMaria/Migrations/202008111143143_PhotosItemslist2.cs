﻿namespace SunMaria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhotosItemslist2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Photos", "ItemId", c => c.Int(nullable: false));
            DropColumn("dbo.Photos", "PhotoId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Photos", "PhotoId", c => c.Int(nullable: false));
            DropColumn("dbo.Photos", "ItemId");
        }
    }
}
