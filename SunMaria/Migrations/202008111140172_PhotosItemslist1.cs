﻿namespace SunMaria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhotosItemslist1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Photos", "Items_Id", c => c.Int());
            CreateIndex("dbo.Photos", "Items_Id");
            AddForeignKey("dbo.Photos", "Items_Id", "dbo.Items", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Photos", "Items_Id", "dbo.Items");
            DropIndex("dbo.Photos", new[] { "Items_Id" });
            DropColumn("dbo.Photos", "Items_Id");
        }
    }
}
