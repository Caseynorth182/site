﻿namespace SunMaria.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPhotoPath : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "PhotoPath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Items", "PhotoPath");
        }
    }
}
